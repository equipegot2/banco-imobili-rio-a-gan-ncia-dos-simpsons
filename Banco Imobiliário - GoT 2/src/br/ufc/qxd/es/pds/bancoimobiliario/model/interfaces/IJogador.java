package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;


public interface IJogador extends IJogadorObservavel {

	public void deslocadoPara(int posicao);
	
	public void movimentadoPara(int posicao);
	
	public int posicaoAtual();
	
	public float saldo();
	
	public void adicionarFundos(float valor);
	
	public void debitarFundos(float valor);
	
	public String nome();

	public Iterable<ILogradouro> listarPropriedades();
	
	public void novaPropriedade(ILogradouro logradouro);
	
	public String urlImage();
}
