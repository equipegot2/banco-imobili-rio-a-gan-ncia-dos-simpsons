package br.ufc.qxd.es.pds.bancoimobiliario.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.IdentificadorDeJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IBancoImobiliario;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IControladorDeTurnos;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IObeservadorDeControladorDeTurnos;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IObservadorDeJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IPainelInformativo;
import br.ufc.qxd.es.pds.bancoimobiliario.view.listeners.BotaoFazerJogadaListener;
import br.ufc.qxd.es.pds.bancoimobiliario.view.listeners.BotaoIniciarPartidaListener;
import br.ufc.qxd.es.pds.bancoimobiliario.view.listeners.BotaoTerminarTurnoListener;
import br.ufc.qxd.es.pds.bancoimobiliario.view.listeners.PortifolioListener;
import br.ufc.qxd.es.pds.bancoimobiliario.view.listeners.CasaClicadaListener;

public class TelaPrincipal implements IObeservadorDeControladorDeTurnos, IObservadorDeJogador{

	/**
	 * Create the panel.
	 */
	private final int larguraDasCasas;
	private final int alturaDasCasas;
	private JPanel tela;
	private JButton botaoIniciarPartida;

	private JButton botaoTerminarTurno;
	private JButton botaoFazerJogada;
	
	private JLabel labelTextoJogador1;
	private JLabel labelFotoJogador1;
	private JButton botaoLocalAtualJogador1;
	private JLabel labelTextoSaldoJogador1;
	private JLabel labelValorSaldoJogador1;
	private JButton botaoVerPropriedadesJogador1;
	private JLabel labelTextoLocalAtualJogador1;
	private JLabel labelTextoJogador2;
	private JLabel labelTextoLocalAtualJogador2;
	private JButton botaoPosioAtualDoJogador2;
	private JLabel labelFotoJogador2;
	private JLabel labelTextoSaldoJogador2;
	private JLabel labelValorSaldoJogador2;
	private JButton botaoVerPropriedadesJogador2;
	private JLabel labelTextoJogadorDaVez;
	private JLabel labelFotoJogadorDaVez;
	private JPanel painelCentralLogradouros;

	private IBancoImobiliario banco;
	private IControladorDeTurnos controladorDeTurnos;
	private IPainelInformativo painelInformativo;
	private List<JButton> casas;
	
	private List<IJogador> jogadores;
	private IJogador jogador1;
	private IJogador jogador2;
	
	public TelaPrincipal(IBancoImobiliario banco,IControladorDeTurnos controladorDeTurnos) {
		
		larguraDasCasas = 91;
		alturaDasCasas = 78;
		
		tela = new JPanel();
		casas = new ArrayList<JButton>();
		
		this.banco = banco;
		this.controladorDeTurnos = controladorDeTurnos;
		controladorDeTurnos.registerControladorDeTurnosObserver(this);
		this.jogadores = controladorDeTurnos.listarJogadores();
		
		tela.setBackground(Color.YELLOW);
		tela.setLayout(null);
		
		registrarOberverDeJogadores();
		
		painelInformativo = new PainelInformativoPontoDePartida();
		painelInformativo.montar();
		painelCentralLogradouros = painelInformativo.getTela();
		tela.add(painelCentralLogradouros);
		
		setJogadores();	
		
		JSeparator separadorBaseMenu = new JSeparator();
		separadorBaseMenu.setBounds(0, 544, 845, 1);
		tela.add(separadorBaseMenu);
		
		montarCasas();
		addicionarActionsAsCasas();
		montarMenuInferior();
		
		montarMenuJogador1();
		
		JSeparator separadorTopoJogadorDaVez = new JSeparator();
		separadorTopoJogadorDaVez.setBounds(614, 186, 217, 1);
		tela.add(separadorTopoJogadorDaVez);
		
		JSeparator separadorBaseJogadorDaVez = new JSeparator();
		separadorBaseJogadorDaVez.setBounds(614, 351, 217, 1);
		tela.add(separadorBaseJogadorDaVez);
		
		montarMenuJogador2();
		
		montarTelaJogadorDaVez();
	}

	@Override
	public void novoJogadorDaVez(IControladorDeTurnos controlador) {
		IJogador jogador = controlador.jogadorDaVez();
		labelFotoJogadorDaVez.setIcon(new ImageIcon(TelaPrincipal.class.getResource(jogador.urlImage())));
		int casa = jogador.posicaoAtual();
		casas.get(casa).doClick();
	}
	
	
	
	public void atualizarPainelInformativo(IPainelInformativo novoPainel){
		painelInformativo = novoPainel;
		tela.remove(painelCentralLogradouros);
		painelCentralLogradouros = painelInformativo.getTela();
		tela.add(painelCentralLogradouros);
		tela.repaint();
	}
	
	public JPanel getTela() {
		return tela;
	}
	
	public void iniciarPartida(){
		botaoIniciarPartida.setEnabled(false);
		botaoFazerJogada.setEnabled(true);
	}
	
	public void desabilitarCompra(){
		painelInformativo.desabilitarCompra();
		atualizarPainelInformativo(painelInformativo);
	}
	
	private void montarTelaJogadorDaVez() {
		labelTextoJogadorDaVez = new JLabel("SUA VEZ:");
		labelTextoJogadorDaVez.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoJogadorDaVez.setBounds(625, 266, larguraDasCasas, 14);
		tela.add(labelTextoJogadorDaVez);
		
		labelFotoJogadorDaVez = new JLabel();
		labelFotoJogadorDaVez.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/fotoJogador1.png")));
		labelFotoJogadorDaVez.setBounds(726, 231, larguraDasCasas, alturaDasCasas);
		tela.add(labelFotoJogadorDaVez);
	}

	private void montarMenuJogador2() {
		labelValorSaldoJogador2 = new JLabel();
		labelTextoSaldoJogador2 = new JLabel("SALDO R$:");
		labelFotoJogador2 = new JLabel();
		
		botaoPosioAtualDoJogador2 = new JButton("EXIBIR");
		botaoPosioAtualDoJogador2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int posicao = jogador2.posicaoAtual();
				casas.get(posicao).doClick();
			}
		});
		
		labelTextoLocalAtualJogador2 = new JLabel("LOCAL ATUAL");
		labelTextoJogador2 = new JLabel("JOGADOR 2");
		botaoVerPropriedadesJogador2 = new JButton("VER PROPRIEDADES");
		
		labelTextoJogador2.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoJogador2.setBounds(624, 374, larguraDasCasas, 14);
		tela.add(labelTextoJogador2);
		
		labelTextoLocalAtualJogador2.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoLocalAtualJogador2.setBounds(725, 374, larguraDasCasas, 14);
		tela.add(labelTextoLocalAtualJogador2);
		
		botaoPosioAtualDoJogador2.setBounds(725, 396, larguraDasCasas, alturaDasCasas);
		tela.add(botaoPosioAtualDoJogador2);
		
		labelFotoJogador2.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/fotoJogador2.png")));
		labelFotoJogador2.setBounds(624, 396, larguraDasCasas, alturaDasCasas);
		tela.add(labelFotoJogador2);
		
		labelTextoSaldoJogador2.setHorizontalAlignment(SwingConstants.RIGHT);
		labelTextoSaldoJogador2.setBounds(624, 485, larguraDasCasas, 14);
		tela.add(labelTextoSaldoJogador2);
		
		String saldo = "";
		saldo += controladorDeTurnos.jogadorDaVez().saldo();
		labelValorSaldoJogador2.setText(saldo);
		labelValorSaldoJogador2.setHorizontalAlignment(SwingConstants.LEFT);
		labelValorSaldoJogador2.setBounds(725, 485, larguraDasCasas, 14);
		tela.add(labelValorSaldoJogador2);
		
		botaoVerPropriedadesJogador2.setBounds(624, 510, 191, 23);
		botaoVerPropriedadesJogador2.addActionListener(new PortifolioListener(jogadores.get(1)));
		tela.add(botaoVerPropriedadesJogador2);
	}

	private void montarMenuJogador1() {
		labelTextoJogador1 = new JLabel("JOGADOR 1");
		labelFotoJogador1 = new JLabel();
		
		botaoLocalAtualJogador1 = new JButton("EXIBIR");
		botaoLocalAtualJogador1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int posicao = jogador1.posicaoAtual();
				casas.get(posicao).doClick();
			}
		});
		
		labelTextoSaldoJogador1 = new JLabel("SALDO R$:");
		labelValorSaldoJogador1 = new JLabel();
		botaoVerPropriedadesJogador1 = new JButton("VER PROPRIEDADES");
		labelTextoLocalAtualJogador1 = new JLabel("LOCAL ATUAL");
		
		labelTextoJogador1.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoJogador1.setBounds(625, 11, larguraDasCasas, 14);
		tela.add(labelTextoJogador1);
		
		labelFotoJogador1.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/fotoJogador1.png")));
		labelFotoJogador1.setBounds(625, 33, larguraDasCasas, alturaDasCasas);
		tela.add(labelFotoJogador1);
		
		botaoLocalAtualJogador1.setBounds(726, 33, larguraDasCasas, alturaDasCasas);
		tela.add(botaoLocalAtualJogador1);
		
		labelTextoSaldoJogador1.setHorizontalAlignment(SwingConstants.RIGHT);
		labelTextoSaldoJogador1.setBounds(625, 122, larguraDasCasas, 14);
		tela.add(labelTextoSaldoJogador1);
		
		String saldo = "";
		saldo += controladorDeTurnos.jogadorDaVez().saldo();
		labelValorSaldoJogador1.setText(saldo);
		labelValorSaldoJogador1.setHorizontalAlignment(SwingConstants.LEFT);
		labelValorSaldoJogador1.setBounds(726, 122, larguraDasCasas, 14);
		tela.add(labelValorSaldoJogador1);
		
		botaoVerPropriedadesJogador1.setBounds(625, 147, 191, 23);
		tela.add(botaoVerPropriedadesJogador1);
		
		labelTextoLocalAtualJogador1.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoLocalAtualJogador1.setBounds(726, 11, larguraDasCasas, 14);
		botaoVerPropriedadesJogador1.addActionListener(new PortifolioListener(jogadores.get(0)));
		tela.add(labelTextoLocalAtualJogador1);
	}

	private void addicionarActionsAsCasas() {
		int casaAtual = 0;
		String imagem;
		CasaClicadaListener casaClicadalistener;
		
		List<ILogradouro> logradouros = (List<ILogradouro>) banco.listarCasas();
		
		casaClicadalistener = new CasaClicadaListener(this, new PainelInformativoPontoDePartida());
		casas.get(casaAtual++).addActionListener(casaClicadalistener);
		
		imagem = "/Imovel Casa do Flanders.png";
		registrarImovelListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		imagem = "/Imovel Casa dos Simpsons.png";
		registrarImovelListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Empresa Krust Burg.png";
		registrarEmpresalListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		casas.get(casaAtual++).addActionListener(new CasaClicadaListener(this, new PainelInformativoCasaNeutraImp()));
		
		imagem = "/Empresa Bar do Moe.png";
		registrarEmpresalListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Empresa Canal6.png";
		registrarEmpresalListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Imovel Casa1.png";
		registrarImovelListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Imovel Casa do Burns.png";
		registrarImovelListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Empresa Usina.png";
		registrarEmpresalListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		casas.get(casaAtual++).addActionListener(new CasaClicadaListener(this, new PainelInformativoCasaDeslocarJogadorImp()));
		
		imagem = "/Imovel Casa2.png";
		registrarImovelListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Empresa Mercadinho do Abu.png";
		registrarEmpresalListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;

		imagem = "/Imovel Casa3.png";
		registrarImovelListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		casas.get(casaAtual++).addActionListener(new CasaClicadaListener(this, new PainelInformativoCasaRevesImp()));
		
		imagem = "/Empresa Pet Shop.png";
		registrarEmpresalListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Imovel Casa4.png";
		registrarImovelListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Empresa Doceria.png";
		registrarEmpresalListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Imovel Casa da Krabapell.png";
		registrarImovelListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
		casaAtual++;
		
		imagem = "/Empresa Cervejaria Duff.png";
		registrarEmpresalListener(casas.get(casaAtual), logradouros.get(casaAtual), imagem);
	}

	private void registrarImovelListener(JButton casa, ILogradouro logradouro, String imagem) {
		IPainelInformativo auxPainel = new PainelInformativoImovelImp(logradouro, imagem, banco);
		CasaClicadaListener casaClicadalistener = new CasaClicadaListener(this, auxPainel);
		casa.addActionListener(casaClicadalistener);
	}
	
	private void registrarEmpresalListener(JButton casa, ILogradouro logradouro, String imagem) {
		IPainelInformativo auxPainel = new PainelInformativoEmpresaImp(logradouro, imagem, banco);
		CasaClicadaListener casaClicadalistener = new CasaClicadaListener(this, auxPainel);
		casa.addActionListener(casaClicadalistener);
	}

	private void montarMenuInferior() {
		botaoIniciarPartida = new JButton("INICIAR PARTIDA");
		botaoTerminarTurno = new JButton("TERMINAR TURNO");
		botaoTerminarTurno = new JButton("TERMINAR TURNO");
		botaoFazerJogada = new JButton("FAZER JOGADA");
		
		botaoIniciarPartida.addActionListener(new BotaoIniciarPartidaListener(this));
		botaoIniciarPartida.setBounds(64, 559, 140, 45);
		tela.add(botaoIniciarPartida);
		
		
		
		botaoTerminarTurno.setBounds(472, 559, 140, 45);
		botaoTerminarTurno.setEnabled(false);
		botaoTerminarTurno.addActionListener(new BotaoTerminarTurnoListener(banco, botaoTerminarTurno, botaoFazerJogada, this));
		tela.add(botaoTerminarTurno);
		
		botaoFazerJogada.setEnabled(false);
		botaoFazerJogada.addActionListener(new BotaoFazerJogadaListener(banco, botaoTerminarTurno, botaoFazerJogada));
		botaoFazerJogada.setBounds(676, 559, 140, 45);
		tela.add(botaoFazerJogada);
	}
	
	private void setJogadores(){
		jogador1 = jogadores.get(0);
		jogador2 = jogadores.get(1);
	}
	
	private void montarCasas(){
		GeradorDeBotoaoDasCasas gerador = new GeradorDeBotoaoDasCasas();
		casas = gerador.getCasas();
		for (JButton casa : casas) {
			tela.add(casa);
		}
	}
	
	private void registrarOberverDeJogadores(){
		for (IJogador jogador : jogadores) {
			jogador.registerJogadorObserver(this);
		}
	}

	@Override
	public void jogadorMovimento(IJogador sujeito) {
		int casa = sujeito.posicaoAtual();
		casas.get(casa).doClick();
		painelInformativo.jogadorChegou(sujeito);
		atualizarPainelInformativo(painelInformativo);
	}
	
	@Override
	public void jogadorDeslocado(IJogador sujeito) {
		int casa = sujeito.posicaoAtual();
		casas.get(casa).doClick();
		painelInformativo.jogadorChegou(sujeito);
		atualizarPainelInformativo(painelInformativo);
	}
	
	
	@Override
	public void saldoAtualizado(IJogador jogadorImp) {
		if (jogadorImp.nome().equals(IdentificadorDeJogador.JOGADOR1.getNome())) {
			labelValorSaldoJogador1.setText("" + jogadorImp.saldo());
		}else {
			labelValorSaldoJogador2.setText("" + jogadorImp.saldo());
		}
	}
}
