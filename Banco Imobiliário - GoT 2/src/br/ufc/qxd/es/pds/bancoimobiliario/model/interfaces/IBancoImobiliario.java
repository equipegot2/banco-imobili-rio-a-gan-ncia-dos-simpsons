package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;

import br.ufc.qxd.es.pds.bancoimobiliario.exceptions.ExcecaoDeSaldoInsuficiente;


public interface IBancoImobiliario {
	
	public void fazerJogada(int qtdPosicoes);
	
	public void fazerCompra() throws ExcecaoDeSaldoInsuficiente;

	public Iterable<ILogradouro> listarCasas();
	
	public void concluirJogada();
	
	public void registrarListenerTabuleiro(IObservadorDeTabuleiro observer);
}
