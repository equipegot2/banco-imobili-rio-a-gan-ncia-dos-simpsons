package br.ufc.qxd.es.pds.bancoimobiliario.view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.view.TelaPortifolio;

public class PortifolioListener implements ActionListener {

	private JFrame janela;
	private TelaPortifolio telaPropriedades;
	private IJogador jogador;
	
	public PortifolioListener(IJogador jogador) {
		janela = new JFrame();
		telaPropriedades = new TelaPortifolio();
		telaPropriedades.montar(jogador);
		this.jogador = jogador;
		
		janela.add(telaPropriedades);
		janela.setSize(420, 395);
		janela.setLocationRelativeTo(null);
		janela.setTitle("Propriedades do " + jogador.nome());
		janela.setResizable(false);
		janela.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		telaPropriedades.montar(jogador);
		janela.setState(JFrame.NORMAL);
		janela.setVisible(true);
	}

}
