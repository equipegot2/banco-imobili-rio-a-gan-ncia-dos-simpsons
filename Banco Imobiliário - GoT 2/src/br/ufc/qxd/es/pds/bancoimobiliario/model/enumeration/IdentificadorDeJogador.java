package br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration;

public enum IdentificadorDeJogador {

	JOGADOR1("Jogador1"), JOGADOR2("Jogador2");

	private String nome;

	IdentificadorDeJogador(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
}
