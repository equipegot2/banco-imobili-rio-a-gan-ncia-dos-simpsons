package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;


public interface ILogradouro {

	public void jogadorChegando(IJogador jogador);

	public void jogadorPassando(IJogador jogador);
	
	public String nome();
	
	public boolean estaAVenda();
	
	public IJogador dono();
	
	public void definirDono(IJogador jogador);
	
	public float valor();

}
