package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;

public interface IPropriedade extends ILogradouro{

	public IJogador dono();
	
	public float preco();
	
	public void comprar(IJogador jogador);
}
