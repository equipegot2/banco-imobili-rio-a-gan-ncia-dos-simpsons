package br.ufc.qxd.es.pds.bancoimobiliario.model;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;

public class LogradouroDeslocamentoImp extends LogradouroAbstrato {

	@Override
	public void jogadorChegando(IJogador jogador) {
		comportamento.aplicarEfeito(jogador);
	}

	@Override
	public void jogadorPassando(IJogador jogador) {

	}
}
