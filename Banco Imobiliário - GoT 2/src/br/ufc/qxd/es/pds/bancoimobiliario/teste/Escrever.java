package br.ufc.qxd.es.pds.bancoimobiliario.teste;

import java.util.HashMap;
import java.util.Map;

import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroEmpresaImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroImovelImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroDeslocamentoImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroNeutroImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroPontoDePartidaImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroRevesImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.comportamento.ComportamentoDebitarSaldo;
import br.ufc.qxd.es.pds.bancoimobiliario.model.comportamento.ComportamentoDeslocamento;
import br.ufc.qxd.es.pds.bancoimobiliario.model.comportamento.ComportamentoIncrementarSaldo;
import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.IdentificadorDeLogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.ValoresLogradouros;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class Escrever {
	public static void main(String[] args) {
		Map<Integer,ILogradouro> logradouro = new HashMap<Integer, ILogradouro>();
		
		LogradouroImovelImp imovel = new LogradouroImovelImp();
		imovel.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.IMOVEIS.getTaxa()));
		imovel.definirEstaAVenda(true);
		imovel.definirNome(IdentificadorDeLogradouro.CASA_DO_FLANDERS.getNome());
		imovel.definirValor(ValoresLogradouros.IMOVEIS.getValor());
		imovel.definirAluguel(ValoresLogradouros.IMOVEIS.getTaxa());
		
		
		logradouro.put(IdentificadorDeLogradouro.CASA_DO_FLANDERS.getValor(), imovel);
		
		imovel = new LogradouroImovelImp();
		imovel.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.IMOVEIS.getTaxa()));
		imovel.definirEstaAVenda(true);
		imovel.definirNome(IdentificadorDeLogradouro.CASA_DOS_SIMPSONS.getNome());
		imovel.definirValor(ValoresLogradouros.IMOVEIS.getValor());
		imovel.definirAluguel(ValoresLogradouros.IMOVEIS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.CASA_DOS_SIMPSONS.getValor(), imovel);
		
		imovel = new LogradouroImovelImp();
		imovel.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.IMOVEIS.getTaxa()));
		imovel.definirEstaAVenda(true);
		imovel.definirNome(IdentificadorDeLogradouro.CASA_CLASSE_POBRE.getNome());
		imovel.definirValor(ValoresLogradouros.IMOVEIS.getValor());
		imovel.definirAluguel(ValoresLogradouros.IMOVEIS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.CASA_CLASSE_POBRE.getValor(), imovel);
		
		imovel = new LogradouroImovelImp();
		imovel.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.IMOVEIS.getTaxa()));
		imovel.definirEstaAVenda(true);
		imovel.definirNome(IdentificadorDeLogradouro.CASA_CLASSE_MUITO_RICA.getNome());
		imovel.definirValor(ValoresLogradouros.IMOVEIS.getValor());
		imovel.definirAluguel(ValoresLogradouros.IMOVEIS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.CASA_CLASSE_MUITO_RICA.getValor(), imovel);
		
		imovel = new LogradouroImovelImp();
		imovel.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.IMOVEIS.getTaxa()));
		imovel.definirEstaAVenda(true);
		imovel.definirNome(IdentificadorDeLogradouro.CASA_DA_KRABAPELL.getNome());
		imovel.definirValor(ValoresLogradouros.IMOVEIS.getValor());
		imovel.definirAluguel(ValoresLogradouros.IMOVEIS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.CASA_DA_KRABAPELL.getValor(), imovel);
		
		imovel = new LogradouroImovelImp();
		imovel.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.IMOVEIS.getTaxa()));
		imovel.definirEstaAVenda(true);
		imovel.definirNome(IdentificadorDeLogradouro.CASA_CLASSE_MEDIA.getNome());
		imovel.definirValor(ValoresLogradouros.IMOVEIS.getValor());
		imovel.definirAluguel(ValoresLogradouros.IMOVEIS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.CASA_CLASSE_MEDIA.getValor(), imovel);
		
		imovel = new LogradouroImovelImp();
		imovel.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.IMOVEIS.getTaxa()));
		imovel.definirEstaAVenda(true);
		imovel.definirNome(IdentificadorDeLogradouro.CASA_CLASSE_RICA.getNome());
		imovel.definirValor(ValoresLogradouros.IMOVEIS.getValor());
		imovel.definirAluguel(ValoresLogradouros.IMOVEIS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.CASA_CLASSE_RICA.getValor(), imovel);
		
		imovel = new LogradouroImovelImp();
		imovel.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.IMOVEIS.getTaxa()));
		imovel.definirEstaAVenda(true);
		imovel.definirNome(IdentificadorDeLogradouro.CASA_DO_BURNS.getNome());
		imovel.definirValor(ValoresLogradouros.IMOVEIS.getValor());
		imovel.definirAluguel(ValoresLogradouros.IMOVEIS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.CASA_DO_BURNS.getValor(), imovel);
		
		LogradouroEmpresaImp empresa = new LogradouroEmpresaImp();
		empresa.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.EMPRESAS.getTaxa()));
		empresa.definirEstaAVenda(true);
		empresa.definirNome(IdentificadorDeLogradouro.EMPRESA_CANAL6.getNome());
		empresa.definirValor(ValoresLogradouros.EMPRESAS.getValor());
		empresa.definirTaxa(ValoresLogradouros.EMPRESAS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.EMPRESA_CANAL6.getValor(), empresa);
		
		empresa = new LogradouroEmpresaImp();
		empresa.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.EMPRESAS.getTaxa()));
		empresa.definirEstaAVenda(true);
		empresa.definirNome(IdentificadorDeLogradouro.EMPRESA_KRUST_BURG.getNome());
		empresa.definirValor(ValoresLogradouros.EMPRESAS.getValor());
		empresa.definirTaxa(ValoresLogradouros.EMPRESAS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.EMPRESA_KRUST_BURG.getValor(), empresa);
		
		empresa = new LogradouroEmpresaImp();
		empresa.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.EMPRESAS.getTaxa()));
		empresa.definirEstaAVenda(true);
		empresa.definirNome(IdentificadorDeLogradouro.EMPRESA_CERVEJARIA_DUFF.getNome());
		empresa.definirValor(ValoresLogradouros.EMPRESAS.getValor());
		empresa.definirTaxa(ValoresLogradouros.EMPRESAS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.EMPRESA_CERVEJARIA_DUFF.getValor(), empresa);
		
		empresa = new LogradouroEmpresaImp();
		empresa.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.EMPRESAS.getTaxa()));
		empresa.definirEstaAVenda(true);
		empresa.definirNome(IdentificadorDeLogradouro.EMPRESA_DOCERIA.getNome());
		empresa.definirValor(ValoresLogradouros.EMPRESAS.getValor());
		empresa.definirTaxa(ValoresLogradouros.EMPRESAS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.EMPRESA_DOCERIA.getValor(), empresa);
		
		empresa = new LogradouroEmpresaImp();
		empresa.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.EMPRESAS.getTaxa()));
		empresa.definirEstaAVenda(true);
		empresa.definirNome(IdentificadorDeLogradouro.EMPRESA_USINA.getNome());
		empresa.definirValor(ValoresLogradouros.EMPRESAS.getValor());
		empresa.definirTaxa(ValoresLogradouros.EMPRESAS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.EMPRESA_USINA.getValor(), empresa);
		
		empresa = new LogradouroEmpresaImp();
		empresa.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.EMPRESAS.getTaxa()));
		empresa.definirEstaAVenda(true);
		empresa.definirNome(IdentificadorDeLogradouro.EMPRESA_PET_SHOP.getNome());
		empresa.definirValor(ValoresLogradouros.EMPRESAS.getValor());
		empresa.definirTaxa(ValoresLogradouros.EMPRESAS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.EMPRESA_PET_SHOP.getValor(), empresa);
		
		empresa = new LogradouroEmpresaImp();
		empresa.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.EMPRESAS.getTaxa()));
		empresa.definirEstaAVenda(true);
		empresa.definirNome(IdentificadorDeLogradouro.EMPRESA_MERCADINHO_DO_ABU.getNome());
		empresa.definirValor(ValoresLogradouros.EMPRESAS.getValor());
		empresa.definirTaxa(ValoresLogradouros.EMPRESAS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.EMPRESA_MERCADINHO_DO_ABU.getValor(), empresa);
		
		empresa = new LogradouroEmpresaImp();
		empresa.definirComportamento(new ComportamentoDebitarSaldo(ValoresLogradouros.EMPRESAS.getTaxa()));
		empresa.definirEstaAVenda(true);
		empresa.definirNome(IdentificadorDeLogradouro.EMPRESA_BAR_DO_MOE.getNome());
		empresa.definirValor(ValoresLogradouros.EMPRESAS.getValor());
		empresa.definirTaxa(ValoresLogradouros.EMPRESAS.getTaxa());
		
		logradouro.put(IdentificadorDeLogradouro.EMPRESA_BAR_DO_MOE.getValor(), empresa);
		
		LogradouroPontoDePartidaImp inicio = new LogradouroPontoDePartidaImp();
		inicio.definirComportamento(new ComportamentoIncrementarSaldo(200.0F));
		inicio.definirNome(IdentificadorDeLogradouro.ESPECIAL_PONTO_DE_PARTIDA.getNome());
		
		logradouro.put(IdentificadorDeLogradouro.ESPECIAL_PONTO_DE_PARTIDA.getValor(), inicio);
		
		LogradouroNeutroImp neutro = new LogradouroNeutroImp();
		neutro.definirComportamento(null);
		neutro.definirNome(IdentificadorDeLogradouro.ESPECIAL_CASA_NEUTRA.getNome());
		
		logradouro.put(IdentificadorDeLogradouro.ESPECIAL_CASA_NEUTRA.getValor(), neutro);
		
		LogradouroDeslocamentoImp deslocamento = new LogradouroDeslocamentoImp();
		deslocamento.definirComportamento(new ComportamentoDeslocamento(5));
		deslocamento.definirNome(IdentificadorDeLogradouro.ESPECIAL_DESLOCAMENTO.getNome());
		
		logradouro.put(IdentificadorDeLogradouro.ESPECIAL_DESLOCAMENTO.getValor(), deslocamento);
		
		LogradouroRevesImp reves = new LogradouroRevesImp();
		reves.definirComportamento(new ComportamentoDebitarSaldo(200));
		reves.definirNome(IdentificadorDeLogradouro.ESPECIAL_CASA_REVES.getNome());
		
		logradouro.put(IdentificadorDeLogradouro.ESPECIAL_CASA_REVES.getValor(), reves);
		
		XStream xStream = new XStream(new DomDriver());
		
		
			xStream = new XStream(new DomDriver());
			xStream.alias("Map", Map.class);
			xStream.alias("Imovel",LogradouroImovelImp.class);
			xStream.alias("Empresa",LogradouroEmpresaImp.class);
			xStream.alias("PontoInical",LogradouroPontoDePartidaImp.class);
			xStream.alias("LogradouroReves", LogradouroRevesImp.class);
			xStream.alias("LogradouroNeuto", LogradouroNeutroImp.class);
			xStream.alias("LogradouroDeslocamento",LogradouroDeslocamentoImp.class);
			xStream.alias("ComportamentoDebitarSaldo", ComportamentoDebitarSaldo.class);
			xStream.alias("ComportamentoDeslocamento", ComportamentoDeslocamento.class);
			xStream.alias("ComportamentoIncrementarSaldo", ComportamentoIncrementarSaldo.class);
			
		
		System.out.println(xStream.toXML(logradouro));
	}
}
