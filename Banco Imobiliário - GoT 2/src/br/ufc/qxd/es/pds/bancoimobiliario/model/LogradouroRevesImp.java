package br.ufc.qxd.es.pds.bancoimobiliario.model;

import javax.swing.JOptionPane;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;

public class LogradouroRevesImp extends LogradouroAbstrato {

	@Override
	public void jogadorChegando(IJogador jogador) {
		comportamento.aplicarEfeito(jogador);
		JOptionPane.showMessageDialog(null, "Você deu azar. Perdeu 200!");
	}

	@Override
	public void jogadorPassando(IJogador jogador) {
		
	}
}
