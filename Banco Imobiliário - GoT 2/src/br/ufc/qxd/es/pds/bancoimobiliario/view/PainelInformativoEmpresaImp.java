package br.ufc.qxd.es.pds.bancoimobiliario.view;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.ValoresLogradouros;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IBancoImobiliario;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IPainelInformativo;
import br.ufc.qxd.es.pds.bancoimobiliario.view.listeners.BotaoComprarListener;

public class PainelInformativoEmpresaImp implements IPainelInformativo {

	private JPanel tela;
	private JLabel labelFotoDoLogradouro;
	private JButton botaoComprar;
	private JLabel labelNomeDoLogradouro;
	private JLabel labelValorDoLogradouro;
	private JLabel labelTaxaDologradouro;
	private JLabel labelFotoDoDonoDoLogradouro;
	private JLabel labelTextoDonoDoLogradouro;
	private JLabel labelDescricaoDoLogradouro;
	private ILogradouro logradouro;
	private String imagem;

	public PainelInformativoEmpresaImp(ILogradouro logradouro, String imagem, IBancoImobiliario banco) {
		this.logradouro = logradouro;
		
		this.imagem = imagem;
		
		tela = new JPanel();
		labelFotoDoLogradouro = new JLabel();
		botaoComprar = new JButton();
		botaoComprar.setEnabled(false);
		botaoComprar.addActionListener(new BotaoComprarListener(banco, botaoComprar));
		labelNomeDoLogradouro = new JLabel();
		labelValorDoLogradouro = new JLabel();
		labelTaxaDologradouro = new JLabel();
		labelFotoDoDonoDoLogradouro = new JLabel();
		labelTextoDonoDoLogradouro = new JLabel();
		labelDescricaoDoLogradouro = new JLabel();
	}
	
	@Override
	public void montar() {
		tela.setBorder(new LineBorder(new Color(0, 0, 255), 5));
		tela.setBounds(109, 101, 396, 343);
		tela.setLayout(null);
		
		labelFotoDoLogradouro.setIcon(new ImageIcon(PainelInformativoEmpresaImp.class.getResource(imagem)));
		labelFotoDoLogradouro.setHorizontalAlignment(SwingConstants.CENTER);
		labelFotoDoLogradouro.setBounds(10, 11, 220, 206);
		tela.add(labelFotoDoLogradouro);
		
		botaoComprar.setText("COMPRAR");
		botaoComprar.setBounds(10, 309, 376, 23);
		tela.add(botaoComprar);
		
		labelNomeDoLogradouro.setText(logradouro.nome());
		labelNomeDoLogradouro.setBounds(240, 11, 146, 14);
		tela.add(labelNomeDoLogradouro);
		
		labelValorDoLogradouro.setText("Valor: " + logradouro.valor());
		labelValorDoLogradouro.setBounds(240, 36, 146, 14);
		tela.add(labelValorDoLogradouro);
		
		labelTaxaDologradouro.setText("Taxa: " + ValoresLogradouros.EMPRESAS.getTaxa());
		labelTaxaDologradouro.setBounds(240, 61, 146, 14);
		tela.add(labelTaxaDologradouro);
		
		labelTextoDonoDoLogradouro.setText("Dono");
		labelTextoDonoDoLogradouro.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoDonoDoLogradouro.setBounds(270, 86, 91, 14);
		tela.add(labelTextoDonoDoLogradouro);
		
		labelFotoDoDonoDoLogradouro.setText("Sem Dono");
		labelFotoDoDonoDoLogradouro.setHorizontalAlignment(SwingConstants.CENTER);
		labelFotoDoDonoDoLogradouro.setBounds(270, 110, 91, 78);
		tela.add(labelFotoDoDonoDoLogradouro);
		
		labelDescricaoDoLogradouro.setText("Descrição: Esta empresa rende taxa de uso para o dono.");
		labelDescricaoDoLogradouro.setHorizontalAlignment(SwingConstants.CENTER);
		labelDescricaoDoLogradouro.setBounds(10, 228, 376, 70);
		tela.add(labelDescricaoDoLogradouro);
	}
	
	@Override
	public JPanel getTela() {
		if (logradouro.dono() != null) {
			String urlImagem = logradouro.dono().urlImage();
			labelFotoDoDonoDoLogradouro.setText("");
			labelFotoDoDonoDoLogradouro.setIcon(new ImageIcon(PainelInformativoPontoDePartida.class.getResource(urlImagem)));
		}
		return tela;
	}
	
	@Override
	public void jogadorChegou(IJogador jogador) {
		if (logradouro.estaAVenda()) {
			botaoComprar.setEnabled(true);
		} else if (jogador.equals(logradouro.dono())) {
			labelDescricaoDoLogradouro.setText("Seja bem vindo a sua casa!");
		}else {
			String mensagem;
			mensagem = "Esta empresa possui dono. Voce terá que pagar taxa de uso.\n";
			mensagem += "Por isso será debitado de seu saldo: R$ " + ValoresLogradouros.EMPRESAS.getTaxa();
			mensagem += "\nEste valor será creditado no saldo do dono";
			JOptionPane.showMessageDialog(null, mensagem);
		}
	}
	
	@Override
	public void desabilitarCompra() {
		botaoComprar.setEnabled(false);
	}
}
