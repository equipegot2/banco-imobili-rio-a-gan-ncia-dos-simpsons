package br.ufc.qxd.es.pds.bancoimobiliario.view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import br.ufc.qxd.es.pds.bancoimobiliario.exceptions.ExcecaoDeSaldoInsuficiente;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IBancoImobiliario;

public class BotaoComprarListener implements ActionListener {

	private IBancoImobiliario banco;
	private JButton botaoComprar;
	public BotaoComprarListener(IBancoImobiliario banco, JButton botaoComprar) {
		this.banco = banco;
		this.botaoComprar = botaoComprar;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		try {
			banco.fazerCompra();
			botaoComprar.setEnabled(false);
		} catch (ExcecaoDeSaldoInsuficiente e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

}
