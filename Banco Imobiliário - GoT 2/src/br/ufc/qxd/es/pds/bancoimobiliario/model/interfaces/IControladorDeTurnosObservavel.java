package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;

public interface IControladorDeTurnosObservavel {

	void registerControladorDeTurnosObserver(
			IObeservadorDeControladorDeTurnos observador);

	void removeControladorDeTurnosObserver(
			IObeservadorDeControladorDeTurnos observador);
}
