package br.ufc.qxd.es.pds.bancoimobiliario.model.comportamento;

import javax.swing.JOptionPane;

import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.IdentificadorDeLogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IComportamento;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;

public class ComportamentoDeslocamento implements IComportamento {

	private final int quantidadeDePosicoes;
	
	public ComportamentoDeslocamento(int quantidadeDePosicoes) {
		this.quantidadeDePosicoes = quantidadeDePosicoes;
	}
	@Override
	public void aplicarEfeito(IJogador jogador) {
		JOptionPane.showMessageDialog(null, "Você caiu na casa deslocamento. Vá para a casa"+ IdentificadorDeLogradouro.EMPRESA_PET_SHOP.getNome());
		jogador.deslocadoPara(quantidadeDePosicoes+jogador.posicaoAtual());
	}
}
