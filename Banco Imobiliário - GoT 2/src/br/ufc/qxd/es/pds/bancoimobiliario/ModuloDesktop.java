package br.ufc.qxd.es.pds.bancoimobiliario;

import br.ufc.qxd.es.pds.bancoimobiliario.construcao.ITabuleiroConstrutor;
import br.ufc.qxd.es.pds.bancoimobiliario.construcao.ITabuleiroDiretor;
import br.ufc.qxd.es.pds.bancoimobiliario.construcao.TabuleiroConstrutorImp;
import br.ufc.qxd.es.pds.bancoimobiliario.construcao.TabuleiroDiretorImp;
import br.ufc.qxd.es.pds.bancoimobiliario.controle.BancoImobiliarioImp;
import br.ufc.qxd.es.pds.bancoimobiliario.controle.ControladorDeTurnosImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IBancoImobiliario;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IControladorDeTurnos;

import com.google.inject.AbstractModule;

public class ModuloDesktop extends AbstractModule{

	@Override
	protected void configure() {
		bind(ITabuleiroDiretor.class).to(TabuleiroDiretorImp.class);
		bind(IBancoImobiliario.class).to(BancoImobiliarioImp.class);
		bind(ITabuleiroConstrutor.class).to(TabuleiroConstrutorImp.class);
		bind(IControladorDeTurnos.class).to(ControladorDeTurnosImp.class);
	}

}
