package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;

import java.util.List;

public interface ITabuleiro extends ITabuleiroObservavel, IObservadorDeJogador{

	public ILogradouro movimentarJogador(IJogador jogador, int qtdPosicoes);
	
	public void venderLogradouro(IJogador jogador);
	
	public ILogradouro ondeEsta(IJogador jogador);
	
	public List<ILogradouro> listarCasas();
}
