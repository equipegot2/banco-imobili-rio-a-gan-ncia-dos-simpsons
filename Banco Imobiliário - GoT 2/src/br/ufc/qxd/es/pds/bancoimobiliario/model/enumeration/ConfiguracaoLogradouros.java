package br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration;

public enum ConfiguracaoLogradouros {
	QUANTIDADE_DE_LOGRADOUROS (20),
	QUANTIDADE_DE_EMPRESAS (8),
	QUANTIDADE_DE_IMOVEIS(8);
	
	private int valor;
	
	ConfiguracaoLogradouros(int valor){
		this.valor = valor;
	}
	
	public int getValor(){
		return valor;
	}
}
