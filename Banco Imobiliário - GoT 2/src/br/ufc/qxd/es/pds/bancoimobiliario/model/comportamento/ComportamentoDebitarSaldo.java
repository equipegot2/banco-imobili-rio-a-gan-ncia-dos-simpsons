package br.ufc.qxd.es.pds.bancoimobiliario.model.comportamento;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IComportamento;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;

public class ComportamentoDebitarSaldo implements IComportamento{
	private final float valorDoDebito;
	
	public ComportamentoDebitarSaldo(float valor){
		valorDoDebito = valor;
	}
	@Override
	public void aplicarEfeito(IJogador jogador) {
		jogador.debitarFundos(valorDoDebito);
		
	}

}
