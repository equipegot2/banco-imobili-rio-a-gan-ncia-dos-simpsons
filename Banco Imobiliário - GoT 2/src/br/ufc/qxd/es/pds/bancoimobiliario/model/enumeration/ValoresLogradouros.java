package br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration;

public enum ValoresLogradouros {
	IMOVEIS(400.0F, 100.0F),
	EMPRESAS(450.0F, 150.0F);
	
	
	private float valor;
	private float taxa;
	
	ValoresLogradouros(float valor, float taxa) {
		this.taxa = taxa;
		this.valor = valor;
	}
	
	public float getValor(){
		return valor;
	}
	
	public float getTaxa(){
		return taxa;
	}
}
