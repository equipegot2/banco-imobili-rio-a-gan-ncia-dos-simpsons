package br.ufc.qxd.es.pds.bancoimobiliario.exceptions;

public class ExcecaoCarregamentoDeDados extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static String mensagem = "Falha no carregamento dos daods";
	
	public ExcecaoCarregamentoDeDados() {
		// TODO Auto-generated constructor stub
		super(mensagem);
	}
}
