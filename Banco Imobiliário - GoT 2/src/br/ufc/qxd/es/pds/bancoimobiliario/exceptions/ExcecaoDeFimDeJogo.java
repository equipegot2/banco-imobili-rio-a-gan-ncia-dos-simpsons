package br.ufc.qxd.es.pds.bancoimobiliario.exceptions;

//TODO talvez trocar o tipo da exception
public class ExcecaoDeFimDeJogo extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static String mensagem = "Fim de jogo";
	
	public ExcecaoDeFimDeJogo(String nome) {
		super(mensagem + " " + nome + " Perdeu!");
	}
}
