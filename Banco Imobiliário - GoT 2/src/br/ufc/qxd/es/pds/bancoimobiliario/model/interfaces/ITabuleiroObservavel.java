package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;

public interface ITabuleiroObservavel {

	public void registrarObservadorDeTabuleiro(IObservadorDeTabuleiro observador);

	public void removerTabuleiroObserver(IObservadorDeTabuleiro observador);
}