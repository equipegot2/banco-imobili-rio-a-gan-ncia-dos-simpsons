package br.ufc.qxd.es.pds.bancoimobiliario.view;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class GeradorDeBotoaoDasCasas {

	private JButton casa;
	
	private List<JButton> casas;
	private int larguraDasCasas;
	private int alturaDasCasas;
	
	public GeradorDeBotoaoDasCasas() {
		this.larguraDasCasas = 91;
		this.alturaDasCasas = 78;
		
		casas = new ArrayList<JButton>();
		
		configurarCasaPontoDePartida();
		configurarCasaDoFlanders();
		configurarCasaDosSimpsons();
		configurarEmpresaKrustBug();
		configurarCasaNeutra();
		configurarEmpresaBarDoMoe();
		configurarEmpresaCanal5();
		configurarCasaPobre();
		configurarCasaDoBurns();
		configurarEmpresaUsina();
		configurarCasaDeslocamento();
		configurarCasaRica();
		configurarEmpresaMercadinhoDoAbu();
		configurarCasaMedia();
		configurarCasaReves();
		configurarEmpresaPetShop();
		configurarCasaMuitoRica();
		configurarEmpresaDoceria();
		configurarCasaDaKrabapell();
		configurarEmpresaCervejariaDuff();
	}

	public List<JButton> getCasas(){
		return casas;
	}
	
	private void configurarCasaPontoDePartida() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Especial Ponto de Partida.png")));
		casa.setBounds(10, 455, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaDoFlanders() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Imovel Casa do Flanders.png")));
		casa.setBounds(10, 366, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaDosSimpsons() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Imovel Casa dos Simpsons.png")));
		casa.setBounds(10, 275, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}

	private void configurarEmpresaKrustBug() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Empresa Krust Burger.png")));
		casa.setBounds(10, 186, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaNeutra() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Especial Casa Neutra.png")));
		casa.setBounds(10, 100, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarEmpresaBarDoMoe() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Empresa Bar do Moe.png")));
		casa.setBounds(10, 11, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarEmpresaCanal5() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Empresa Canal 6.png")));
		casa.setBounds(111, 11, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaPobre() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Imovel Casa1.png")));
		casa.setBounds(214, 11, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaDoBurns() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Imovel Mansão do Burns.png")));
		casa.setBounds(315, 11, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarEmpresaUsina() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Empresa Usina.png")));
		casa.setBounds(416, 11, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaDeslocamento() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Especial Deslocamento.png")));
		casa.setBounds(515, 11, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaRica() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Imovel Casa2.png")));
		casa.setBounds(515, 100, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarEmpresaMercadinhoDoAbu() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Empresa Mercadinho do Abu.png")));
		casa.setBounds(515, 186, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaMedia() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Imovel Casa3.png")));
		casa.setBounds(515, 275, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaReves() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Especial Reves.png")));
		casa.setBounds(515, 366, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarEmpresaPetShop() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Empresa Petshop.png")));
		casa.setBounds(515, 455, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaMuitoRica() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Imovel Casa4.png")));
		casa.setBounds(414, 455, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarEmpresaDoceria() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Empresa Doceria.png")));
		casa.setBounds(313, 455, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarCasaDaKrabapell() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Imovel Casa da Krabapel.png")));
		casa.setBounds(212, 455, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
	
	private void configurarEmpresaCervejariaDuff() {
		casa = new JButton();
		casa.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icones/Empresa Cervejaria Duff.png")));
		casa.setBounds(111, 455, larguraDasCasas, alturaDasCasas);
		casas.add(casa);
	}
}
