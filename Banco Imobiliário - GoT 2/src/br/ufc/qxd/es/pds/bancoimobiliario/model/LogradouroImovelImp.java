package br.ufc.qxd.es.pds.bancoimobiliario.model;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;

public class LogradouroImovelImp extends LogradouroAbstrato {
	
	private float aluguel;
	
	@Override
	public void jogadorChegando(IJogador jogador) {
		if (!jogador.equals(this.dono()) && dono() != null) {
			cobrarAluguel(jogador);
		}
	}

	@Override
	public void jogadorPassando(IJogador jogador) {
	}
	
	public float aluguel(){
		return aluguel;
	}
	
	public void definirAluguel(float valor){
		this.aluguel = valor;
	}
	
	public void cobrarAluguel(IJogador jogador) {
		comportamento.aplicarEfeito(jogador);
		renderAluguel();
	}
	
	public void renderAluguel(){
		dono().adicionarFundos(aluguel);
	}
}
