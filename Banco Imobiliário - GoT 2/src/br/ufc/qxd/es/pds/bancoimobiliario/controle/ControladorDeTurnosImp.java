package br.ufc.qxd.es.pds.bancoimobiliario.controle;

import java.util.ArrayList;
import java.util.List;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IControladorDeTurnos;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IObeservadorDeControladorDeTurnos;

import com.google.inject.Singleton;

@Singleton
public class ControladorDeTurnosImp implements IControladorDeTurnos {

	private List<IObeservadorDeControladorDeTurnos> observadores;
	private List<IJogador> jogadores;
	private IJogador atual;
	
	public ControladorDeTurnosImp() {
		observadores = new ArrayList<IObeservadorDeControladorDeTurnos>();
	}
	
	@Override
	public IJogador jogadorDaVez() {
		return atual;
	}

	@Override
	public void finalizarTurno() {
		jogadores.remove(0);
		jogadores.add(atual);
		this.atual = jogadores.get(0);
		notifyObserverTurnoMudou();
	}

	@Override
	public void iniciar(List<IJogador> jogadores) {
		this.jogadores = jogadores;
		this.atual = jogadores.get(0);
		notifyObserverTurnoMudou();
	}
	
	@Override
	public void registerControladorDeTurnosObserver(IObeservadorDeControladorDeTurnos observer) {
		observadores.add(observer);
	}
	
	@Override
	public void removeControladorDeTurnosObserver(IObeservadorDeControladorDeTurnos observer) {
		observadores.remove(observer);
	}
	
	@Override
	public List<IJogador> listarJogadores() {
		return jogadores;
	}
	
	private void notifyObserverTurnoMudou() {
		for (IObeservadorDeControladorDeTurnos observer : observadores) {
			observer.novoJogadorDaVez(this);
		}
	}
}
