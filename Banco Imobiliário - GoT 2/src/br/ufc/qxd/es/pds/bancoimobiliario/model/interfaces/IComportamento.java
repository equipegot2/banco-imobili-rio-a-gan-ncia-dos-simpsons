package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;

public interface IComportamento {

	public void aplicarEfeito(IJogador jogador);
}
