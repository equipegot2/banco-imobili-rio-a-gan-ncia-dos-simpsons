package br.ufc.qxd.es.pds.bancoimobiliario.construcao;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ITabuleiro;

public interface ITabuleiroConstrutor {
	public void adicionarLogradouro(ILogradouro logradouro);

	public ITabuleiro getTabuleiro();
}
