package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;

import javax.swing.JPanel;

public interface IPainelInformativo {

	public void montar();
	
	public JPanel getTela();
	
	public void jogadorChegou(IJogador jogador);
	
	public void desabilitarCompra();
	
}
