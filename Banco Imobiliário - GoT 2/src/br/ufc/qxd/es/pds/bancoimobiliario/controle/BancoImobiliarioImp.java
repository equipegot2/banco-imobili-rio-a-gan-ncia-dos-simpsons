package br.ufc.qxd.es.pds.bancoimobiliario.controle;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.ufc.qxd.es.pds.bancoimobiliario.construcao.ITabuleiroDiretor;
import br.ufc.qxd.es.pds.bancoimobiliario.exceptions.ExcecaoDeFimDeJogo;
import br.ufc.qxd.es.pds.bancoimobiliario.exceptions.ExcecaoDeSaldoInsuficiente;
import br.ufc.qxd.es.pds.bancoimobiliario.model.JogadorImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.IdentificadorDeJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IBancoImobiliario;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IControladorDeTurnos;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IObservadorDeTabuleiro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ITabuleiro;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class BancoImobiliarioImp implements IBancoImobiliario {
	
	private ITabuleiro tabuleiro;

	private IControladorDeTurnos controladorDeTurnos;

	private ITabuleiroDiretor tabuleiroDirector;

	private String urlImageJogaador1;

	private String urlImageJogaador2;

	private float fundoIniciais;
	
	private JogadorImp jogador1;
	private JogadorImp jogador2;
	
	@Inject
	public BancoImobiliarioImp(IControladorDeTurnos controladorDeTurnos, ITabuleiroDiretor tabuleiroDirector) {
		
		this.jogador1 = new JogadorImp();
		this.jogador2 = new JogadorImp();
		
		this.controladorDeTurnos = controladorDeTurnos;
		
		this.urlImageJogaador1 = "/icones/fotoJogador1.png";
		this.urlImageJogaador2 = "/icones/fotoJogador2.png";
		this.fundoIniciais = 1_000.0F;
		
		//TODO resoolver pendencia de excecao
		
		this.tabuleiroDirector = tabuleiroDirector;
		
		this.tabuleiroDirector.construir();
		
		this.tabuleiro = tabuleiroDirector.getTabuleiro();
		
		prepararJogadores();
	}


	@Override
	public void fazerJogada(int qtdPosicoes) {
		IJogador jogadorDaVez = controladorDeTurnos.jogadorDaVez();
		
		try{
		
			ILogradouro logradouro = tabuleiro.movimentarJogador(jogadorDaVez, qtdPosicoes);
			if(logradouro.estaAVenda() && jogadorDaVez.saldo() > logradouro.valor() ){
				notifyObservadorCompra(logradouro);
			}
		
		}catch(ExcecaoDeFimDeJogo fimDeJogo){
			JOptionPane.showMessageDialog(null, fimDeJogo.getMessage());
			System.exit(0);
		}
	}

	@Override
	public void fazerCompra() throws ExcecaoDeSaldoInsuficiente{
		
		IJogador jogador = controladorDeTurnos.jogadorDaVez();
		
		float saldo = jogador.saldo();
		float valorDeCompra = tabuleiro.ondeEsta(jogador).valor();
		
		if (saldo < valorDeCompra) {
			throw new ExcecaoDeSaldoInsuficiente();
		}
		
		tabuleiro.venderLogradouro(jogador);
	}

	@Override
	public Iterable<ILogradouro> listarCasas() {
		return tabuleiro.listarCasas();
	}

	@Override
	public void concluirJogada() {
		controladorDeTurnos.finalizarTurno();
	}
	
	private void prepararJogadores() {
		List<IJogador>jogadores = new ArrayList<IJogador>();
		
		jogador1.definirNome(IdentificadorDeJogador.JOGADOR1.getNome());
		jogador1.definirUrlImage(urlImageJogaador1);
		jogador1.adicionarFundos(fundoIniciais);
		jogadores.add(jogador1);
		

		jogador2 = new JogadorImp();
		jogador2.definirNome(IdentificadorDeJogador.JOGADOR2.getNome());
		jogador2.definirUrlImage(urlImageJogaador2);
		jogador2.adicionarFundos(fundoIniciais);
		jogadores.add(jogador2);
		
		this.jogador1.registerJogadorObserver(tabuleiro);
		this.jogador2.registerJogadorObserver(tabuleiro);
		
		controladorDeTurnos.iniciar(jogadores);
	}
	
	@Override
	public void registrarListenerTabuleiro(IObservadorDeTabuleiro observer) {
		this.tabuleiro.registrarObservadorDeTabuleiro(observer);
		
	}

	private void notifyObservadorCompra(ILogradouro logradouro) {
		// TODO Auto-generated method stub
		
	}
}
