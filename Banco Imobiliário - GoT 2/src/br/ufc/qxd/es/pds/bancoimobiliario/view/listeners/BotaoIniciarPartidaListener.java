package br.ufc.qxd.es.pds.bancoimobiliario.view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import br.ufc.qxd.es.pds.bancoimobiliario.view.TelaPrincipal;

public class BotaoIniciarPartidaListener implements ActionListener {

	private TelaPrincipal telaPrincipal;

	public BotaoIniciarPartidaListener(TelaPrincipal telaPrincipal) {
		this.telaPrincipal = telaPrincipal;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int op = JOptionPane.showConfirmDialog(null, "Jogar agora?");
		if (op == JOptionPane.YES_OPTION) {
			telaPrincipal.iniciarPartida();
		}
	}

}
