package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;


public interface IObeservadorDeControladorDeTurnos {

	public void novoJogadorDaVez(IControladorDeTurnos controlador);
}
