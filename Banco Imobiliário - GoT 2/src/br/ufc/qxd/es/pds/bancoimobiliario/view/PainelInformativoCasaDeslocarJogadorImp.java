package br.ufc.qxd.es.pds.bancoimobiliario.view;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.IdentificadorDeLogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IPainelInformativo;

public class PainelInformativoCasaDeslocarJogadorImp implements IPainelInformativo {

	private JPanel tela;
	private JLabel labelFotoDoLogradouro;
	private JLabel labelNomeDoLogradouro;
	private JLabel labelDescricaoDoLogradouro;
	private String urlImagem;
	private String nome;
	
	public PainelInformativoCasaDeslocarJogadorImp(ILogradouro logradouro){
		tela = new JPanel();
		this.urlImagem = "/Especial Deslocamento.jpg";
		this.nome = logradouro.nome();
		labelFotoDoLogradouro = new JLabel();
		labelNomeDoLogradouro = new JLabel();
		labelDescricaoDoLogradouro = new JLabel();
	}
	
	public PainelInformativoCasaDeslocarJogadorImp() {
		tela = new JPanel();
		this.urlImagem = "/Especial Deslocamento.jpg";
		this.nome = "Deslocamento";
		labelFotoDoLogradouro = new JLabel();
		labelNomeDoLogradouro = new JLabel();
		labelDescricaoDoLogradouro = new JLabel();
	}

	@Override
	public void montar() {
		tela.setBorder(new LineBorder(new Color(0, 0, 255), 5));
		tela.setBounds(109, 101, 396, 343);
		tela.setLayout(null);
		
		ImageIcon imagemIcone = new ImageIcon(PainelInformativoCasaDeslocarJogadorImp.class.getResource(urlImagem));
		labelFotoDoLogradouro.setIcon(imagemIcone);
		labelFotoDoLogradouro.setHorizontalAlignment(SwingConstants.CENTER);
		labelFotoDoLogradouro.setBounds(10, 11, 220, 206);
		tela.add(labelFotoDoLogradouro);
		
		labelNomeDoLogradouro.setText(nome);
		labelNomeDoLogradouro.setBounds(240, 11, 146, 14);
		tela.add(labelNomeDoLogradouro);
		
		String descricao = "Ao cair aqui você será enviado para: " + IdentificadorDeLogradouro.EMPRESA_PET_SHOP.getNome();
		labelDescricaoDoLogradouro.setText(descricao);
		labelDescricaoDoLogradouro.setHorizontalAlignment(SwingConstants.CENTER);
		labelDescricaoDoLogradouro.setBounds(10, 228, 376, 70);
		tela.add(labelDescricaoDoLogradouro);
	}
	
	@Override
	public JPanel getTela() {
		return tela;
	}
	
	@Override
	public void jogadorChegou(IJogador jogador) {
		
	}
	
	@Override
	public void desabilitarCompra() {
	}
}
