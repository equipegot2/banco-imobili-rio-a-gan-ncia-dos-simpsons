package br.ufc.qxd.es.pds.bancoimobiliario.view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IPainelInformativo;
import br.ufc.qxd.es.pds.bancoimobiliario.view.TelaPrincipal;

public class CasaClicadaListener implements ActionListener{

	private TelaPrincipal tela;
	private IPainelInformativo painel;
	
	public CasaClicadaListener(TelaPrincipal tela, IPainelInformativo painel) {
		this.tela = tela;
		this.painel = painel;
		painel.montar();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		tela.atualizarPainelInformativo(painel);
	}
}
