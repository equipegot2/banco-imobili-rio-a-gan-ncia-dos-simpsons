package br.ufc.qxd.es.pds.bancoimobiliario.model;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IComportamento;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;

public abstract class LogradouroAbstrato implements ILogradouro {

	private String nome;
	private boolean estaAVenda;
	protected IComportamento comportamento;
	protected IJogador dono;
	private float valor;

	@Override
	public String nome() {
		return this.nome;
	}

	public void definirNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean estaAVenda() {
		return this.estaAVenda;
	}

	public void definirEstaAVenda(boolean vendendo) {
		this.estaAVenda = vendendo;
	}


	public void definirComportamento(IComportamento comportamento) {
		this.comportamento = comportamento;
	}

	@Override
	public IJogador dono() {
		return this.dono;
	}

	@Override
	public void definirDono(IJogador jogador) {
		this.dono = jogador;
		this.estaAVenda = false;
	}

	@Override
	public float valor() {
		return this.valor;
	}

	public void definirValor(float valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		String logradouro = "\n\n";
		
		logradouro += "Nome: " +this.nome + "\n";
		logradouro += "Dono: " + dono().nome() + "\n";
		
		return logradouro;
	}
}
