package br.ufc.qxd.es.pds.bancoimobiliario.view;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IPainelInformativo;

public class PainelInformativoPontoDePartida implements IPainelInformativo{
	private JPanel tela;
	private JLabel labelFotoDoLogradouro;
	private JLabel labelNomeDoLogradouro;
	private JLabel labelDescricaoDoLogradouro;
	private String urlImagem;
	
	public PainelInformativoPontoDePartida() {
		tela = new JPanel();
		this.urlImagem = "/Especial Ponto de Partida.png";
		labelFotoDoLogradouro = new JLabel();
		labelNomeDoLogradouro = new JLabel();
		labelDescricaoDoLogradouro = new JLabel();
	}

	@Override
	public void montar() {
		tela.setBorder(new LineBorder(new Color(0, 0, 255), 5));
		tela.setBounds(109, 101, 396, 343);
		tela.setLayout(null);
		
		ImageIcon imagemIcon = new ImageIcon(PainelInformativoCasaDeslocarJogadorImp.class.getResource(urlImagem));
		labelFotoDoLogradouro.setIcon(imagemIcon);
		labelFotoDoLogradouro.setHorizontalAlignment(SwingConstants.CENTER);
		labelFotoDoLogradouro.setBounds(10, 11, 220, 206);
		tela.add(labelFotoDoLogradouro);
		
		labelNomeDoLogradouro.setText("Springfield");
		labelNomeDoLogradouro.setBounds(240, 11, 146, 14);
		tela.add(labelNomeDoLogradouro);
		
		labelDescricaoDoLogradouro.setText("Descrição: Ao passar aqui, voce ganha 200!");
		labelDescricaoDoLogradouro.setHorizontalAlignment(SwingConstants.CENTER);
		labelDescricaoDoLogradouro.setBounds(10, 228, 376, 70);
		tela.add(labelDescricaoDoLogradouro);
	}
	
	@Override
	public JPanel getTela() {
		return tela;
	}
	
	@Override
	public void jogadorChegou(IJogador jogador) {
		
	}
	
	@Override
	public void desabilitarCompra() {
	}
}
