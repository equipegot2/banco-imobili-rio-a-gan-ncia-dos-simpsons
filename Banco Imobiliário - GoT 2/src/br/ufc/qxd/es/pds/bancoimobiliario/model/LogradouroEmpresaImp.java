package br.ufc.qxd.es.pds.bancoimobiliario.model;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;

public class LogradouroEmpresaImp extends LogradouroAbstrato {

	private float taxa;
	
	@Override
	public void jogadorChegando(IJogador jogador) {
		if (!jogador.equals(this.dono()) && dono() != null) {
			cobrarTaxa(jogador);
			dono().adicionarFundos(taxa);
		}
	}

	@Override
	public void jogadorPassando(IJogador jogador) {

	}

	public float taxa(){
		return taxa;
	}
	
	public void definirTaxa(float valor){
		this.taxa =  valor;
	}
	
	public void cobrarTaxa(IJogador jogador){
		comportamento.aplicarEfeito(jogador);
		renderLucro();
	}
	
	public void renderLucro(){
		dono().adicionarFundos(taxa);
	}
}
