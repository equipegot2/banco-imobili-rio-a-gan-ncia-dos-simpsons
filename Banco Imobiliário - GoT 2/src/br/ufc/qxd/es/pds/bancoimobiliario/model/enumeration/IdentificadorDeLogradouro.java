package br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration;

public enum IdentificadorDeLogradouro {
	ESPECIAL_PONTO_DE_PARTIDA(1, "Ponto de Partida"),
	CASA_DO_FLANDERS(2, "Casa do Flanders"),
	CASA_DOS_SIMPSONS(3, "Casa dos Simpsons"),
	CASA_CLASSE_POBRE(8, "Casa Classe Pobre"),
	CASA_DO_BURNS(9, "Mansão do Burns"),
	CASA_CLASSE_RICA(12, "Casa Classe Rica"),
	CASA_CLASSE_MEDIA(14, "Casa Classe Media"),
	CASA_CLASSE_MUITO_RICA(17, "Casa Classe Muito Rica"),
	CASA_DA_KRABAPELL(19, "Casa da Krabapell"),
	EMPRESA_KRUST_BURG(4, "Krust Burg"),
	EMPRESA_BAR_DO_MOE(6, "Bar do Moe"),
	EMPRESA_CANAL6(7, "Canal 6"),
	EMPRESA_USINA(10, "Usina"),
	EMPRESA_MERCADINHO_DO_ABU(13, "Mercadinho do Abu"),
	EMPRESA_PET_SHOP(16, "Pet Shop"),
	EMPRESA_DOCERIA(18, "Doceria"),
	EMPRESA_CERVEJARIA_DUFF(20, "Cervejaria Duffs"),
	ESPECIAL_DESLOCAMENTO(11, "Deslocamento"),
	ESPECIAL_CASA_NEUTRA(5, "Casa Neutra"),
	ESPECIAL_CASA_REVES(15, "Casa de Reves");
	
	private int valor;
	private String nome;
	
	IdentificadorDeLogradouro(int valor, String nome) {
		
		this.valor = valor;
		this.nome = nome;
	}
	
	public int getValor(){
		
		return valor;
	}
	
	public String getNome(){
		return nome;
	}
}
