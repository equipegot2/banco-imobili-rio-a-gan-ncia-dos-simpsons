package br.ufc.qxd.es.pds.bancoimobiliario.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.ConfiguracaoLogradouros;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IObservadorDeTabuleiro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ITabuleiro;

public class TabuleiroImp implements ITabuleiro{
	
	private List<ILogradouro> logradouros;
	private List<IObservadorDeTabuleiro> observers;
	
	public TabuleiroImp() {
		observers = new ArrayList<IObservadorDeTabuleiro>();
		logradouros = new ArrayList<ILogradouro>();
	}

	@Override
	public ILogradouro movimentarJogador(IJogador jogador, int qtdPosicoes) {
		
		int posicao = jogador.posicaoAtual();
		
		for(int i = 1; i < qtdPosicoes; i++){
			posicao++;
			
			posicao %= ConfiguracaoLogradouros.QUANTIDADE_DE_LOGRADOUROS.getValor();
			logradouros.get(posicao).jogadorPassando(jogador);
		}
		
		posicao++;
		
		posicao %= ConfiguracaoLogradouros.QUANTIDADE_DE_LOGRADOUROS.getValor();
		
		jogador.movimentadoPara(posicao);
		
		logradouros.get(posicao).jogadorChegando(jogador);
		
		return logradouros.get(posicao);
	}

	@Override
	public void venderLogradouro(IJogador jogador) {
		ILogradouro logradouro;
		logradouro = logradouros.get(jogador.posicaoAtual());

		if(logradouro.estaAVenda()){
		
			jogador.debitarFundos(logradouro.valor());
			jogador.novaPropriedade(logradouro);
			logradouro.definirDono(jogador);
		}
	}

	@Override
	public ILogradouro ondeEsta(IJogador jogador) {
		return logradouros.get(jogador.posicaoAtual());
	}

	@Override
	public List<ILogradouro> listarCasas() {
		return Collections.unmodifiableList(logradouros);
	}

	public void adicionarLogradouro(ILogradouro logradouro){
		this.logradouros.add(logradouro);
	}

	
	@Override
	public void registrarObservadorDeTabuleiro(IObservadorDeTabuleiro observador) {
		observers.add(observador);
	}
	
	@Override
	public void removerTabuleiroObserver(IObservadorDeTabuleiro observador) {
		observers.add(observador);
	}

	@Override
	public void jogadorDeslocado(IJogador jogador) {
		logradouros.get(jogador.posicaoAtual()).jogadorChegando(jogador);
		
	}

	@Override
	public void saldoAtualizado(IJogador jogador) {
		
	}

	@Override
	public void jogadorMovimento(IJogador jogador) {
		
	}
}
