package br.ufc.qxd.es.pds.bancoimobiliario.teste;

import javax.swing.JFrame;
import javax.swing.JPanel;

import br.ufc.qxd.es.pds.bancoimobiliario.ModuloDesktop;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IBancoImobiliario;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IControladorDeTurnos;
import br.ufc.qxd.es.pds.bancoimobiliario.view.TelaPrincipal;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {
	public static int JOGAR = 1;
	public static int COMPRAR = 2;
	public static int LISTA_PRPRIEDADES = 3;

	public static void main(String[] args) {
		
		Injector injector = Guice.createInjector(new ModuloDesktop());

		IBancoImobiliario banco = injector.getInstance(IBancoImobiliario.class);
		IControladorDeTurnos controladorDeTurnos = injector.getInstance(IControladorDeTurnos.class);
		
		
		JFrame f = new JFrame();
		JPanel tela = new TelaPrincipal(banco,controladorDeTurnos).getTela();
		f.add(tela);
		f.setSize(860, 655);
		f.setTitle("A Ganancia dos simpsons");
		f.setLocationRelativeTo(null);
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
