package br.ufc.qxd.es.pds.bancoimobiliario.construcao;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ITabuleiro;

public interface ITabuleiroDiretor {
	public void construir();

	public ITabuleiro getTabuleiro();
}
