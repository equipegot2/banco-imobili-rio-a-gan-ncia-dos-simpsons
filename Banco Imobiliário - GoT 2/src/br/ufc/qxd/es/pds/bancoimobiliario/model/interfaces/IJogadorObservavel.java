package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;


public interface IJogadorObservavel {
	void registerJogadorObserver(IObservadorDeJogador observer);
	void removeJogadorObserver(IObservadorDeJogador observer);
}
