package br.ufc.qxd.es.pds.bancoimobiliario.construcao;

import br.ufc.qxd.es.pds.bancoimobiliario.model.TabuleiroImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ITabuleiro;

import com.google.inject.Singleton;

@Singleton
public class TabuleiroConstrutorImp implements ITabuleiroConstrutor{	
	
	private TabuleiroImp tabuleiro;
	
	public TabuleiroConstrutorImp(){
		tabuleiro = new TabuleiroImp();
	}
	
	@Override
	public void adicionarLogradouro(ILogradouro logradouro) {
		tabuleiro.adicionarLogradouro(logradouro);
		
	}	

	@Override
	public ITabuleiro getTabuleiro() {
		return tabuleiro;
	}

}
