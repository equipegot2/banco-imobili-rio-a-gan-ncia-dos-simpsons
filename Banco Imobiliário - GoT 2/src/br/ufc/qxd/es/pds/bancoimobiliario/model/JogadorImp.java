package br.ufc.qxd.es.pds.bancoimobiliario.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.ufc.qxd.es.pds.bancoimobiliario.exceptions.ExcecaoDeFimDeJogo;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IObservadorDeJogador;

public class JogadorImp implements IJogador {
	
	private int posicaoAtual;
	private float saldo;
	private String nome;
	private List<ILogradouro> propriedades;
	private List<IObservadorDeJogador> observadores;
	private String urlImage;
	
	public JogadorImp() {
		propriedades = new ArrayList<ILogradouro>();
		observadores = new ArrayList<IObservadorDeJogador>();
	}
	
	@Override
	public void movimentadoPara(int posicao){
		posicaoAtual = posicao;
		notificarJogadorMovimentado();
	}
	
	@Override
	public void deslocadoPara(int posicao){
		posicaoAtual = posicao;
		notificarJogadorDeslocado();
	}
	
	
	@Override
	public int posicaoAtual() {
		return posicaoAtual;
	}
	
	@Override
	public void adicionarFundos(float valor) {
		this.saldo += valor;
		notificarObsevadorSaldoJogadorMudou();
	}
	
	@Override
	public float saldo() {
		return saldo;
	}
	
	@Override
	public void debitarFundos(float valor) throws ExcecaoDeFimDeJogo {
		if(valor >= this.saldo)
			throw new ExcecaoDeFimDeJogo(nome);
		this.saldo -= valor;
		notificarObsevadorSaldoJogadorMudou();
	}
	
	@Override
	public String nome() {
		return nome;
	}
	
	public void definirNome(String nome) {
		this.nome = nome;
	}
	
	
	@Override
	public Iterable<ILogradouro> listarPropriedades(){
		return Collections.unmodifiableList(propriedades);
	}
	
	@Override
	public void novaPropriedade(ILogradouro logradouro) {
		propriedades.add(logradouro);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JogadorImp) {
			IJogador jogador = (IJogador)obj;
			return jogador.nome().equals(nome);
		}
		return false;
	}


	@Override
	public void registerJogadorObserver(IObservadorDeJogador observador) {
		observadores.add(observador);
		
	}

	@Override
	public void removeJogadorObserver(IObservadorDeJogador observador) {
		observadores.remove(observador);
	}
	
	private void notificarJogadorMovimentado(){
		for (IObservadorDeJogador observador : observadores) {
			observador.jogadorMovimento(this);
		}
	}
	
	private void notificarJogadorDeslocado() {
		for (IObservadorDeJogador observador : observadores) {
			observador.jogadorDeslocado(this);
		}
	}
	
	private void notificarObsevadorSaldoJogadorMudou() {
		for (IObservadorDeJogador observer : observadores) {
			observer.saldoAtualizado(this);
		}
	}
	
	public void definirUrlImage(String urlImage){
		this.urlImage = urlImage;
	}
	
	@Override
	public String urlImage() {
		return urlImage;
	}
}
