package br.ufc.qxd.es.pds.bancoimobiliario.model.comportamento;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IComportamento;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;

public class ComportamentoIncrementarSaldo implements IComportamento {

	private final float bonificacao;

	public ComportamentoIncrementarSaldo(float bonus) {
		bonificacao = bonus;
	}

	@Override
	public void aplicarEfeito(IJogador jogador) {
		jogador.adicionarFundos(bonificacao);
	}
}
