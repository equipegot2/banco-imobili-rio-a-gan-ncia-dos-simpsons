package br.ufc.qxd.es.pds.bancoimobiliario.view;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.IdentificadorDeLogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;

public class TelaPortifolio extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	Iterable<ILogradouro> casas;
	List<JButton> botoes;
	
	public TelaPortifolio() {
		setBackground(Color.YELLOW);
		setLayout(null);
		
		botoes = new ArrayList<JButton>();
		
		criarBotoesImovel();
		criarBotoesEmpresa();

	}

	private void criarBotoesEmpresa() {
		JButton botao;
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Empresa Krust Burger.png")));
		botao.setBounds(10, 189, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.EMPRESA_KRUST_BURG.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Empresa Bar do Moe.png")));
		botao.setBounds(111, 189, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.EMPRESA_BAR_DO_MOE.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Empresa Canal 6.png")));
		botao.setBounds(212, 189, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.EMPRESA_CANAL6.getNome());
		botoes.add(botao);

		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Empresa Usina.png")));
		botao.setBounds(313, 189, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.EMPRESA_USINA.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Empresa Mercadinho do Abu.png")));
		botao.setBounds(10, 278, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.EMPRESA_MERCADINHO_DO_ABU.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Empresa Petshop.png")));
		botao.setBounds(111, 278, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.EMPRESA_PET_SHOP.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Empresa Doceria.png")));
		botao.setBounds(212, 278, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.EMPRESA_DOCERIA.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Empresa Cervejaria Duff.png")));
		botao.setBounds(313, 278, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.EMPRESA_CERVEJARIA_DUFF.getNome());
		botoes.add(botao);
	}

	private void criarBotoesImovel() {
		JButton botao;
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Imovel Casa do Flanders.png")));
		botao.setBounds(10, 11, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.CASA_DO_FLANDERS.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Imovel Casa dos Simpsons.png")));
		botao.setBounds(111, 11, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.CASA_DOS_SIMPSONS.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Imovel Casa1.png")));
		botao.setBounds(212, 11, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.CASA_CLASSE_POBRE.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Imovel Mansão do Burns.png")));
		botao.setBounds(313, 11, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.CASA_DO_BURNS.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Imovel Casa2.png")));
		botao.setBounds(10, 100, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.CASA_CLASSE_RICA.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Imovel Casa3.png")));
		botao.setBounds(111, 100, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.CASA_CLASSE_MEDIA.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Imovel Casa4.png")));
		botao.setBounds(212, 100, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.CASA_CLASSE_MUITO_RICA.getNome());
		botoes.add(botao);
		
		botao = new JButton();
		botao.setIcon(new ImageIcon(TelaPortifolio.class.getResource("/icones/Imovel Casa da Krabapel.png")));
		botao.setBounds(313, 100, 91, 78);
		botao.setActionCommand(IdentificadorDeLogradouro.CASA_DA_KRABAPELL.getNome());
		botoes.add(botao);
	}

	public void montar(IJogador jogador) {
		casas = jogador.listarPropriedades();
		
		for (JButton botao : botoes) {
			botao.setEnabled(false);
			add(botao);
		}
		
		for (ILogradouro propriedade : casas) {
			habilitar(propriedade);
		}
		
	}

	private void habilitar(ILogradouro propriedade) {
		String nome = propriedade.nome();
		for (JButton botao : botoes) {
			if (botao.getActionCommand().equals(nome)) {
				botao.setEnabled(true);
			}
		}
	}
	
	
}
