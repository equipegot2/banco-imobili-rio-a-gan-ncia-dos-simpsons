package br.ufc.qxd.es.pds.bancoimobiliario.exceptions;

public class ExcecaoDeSaldoInsuficiente extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExcecaoDeSaldoInsuficiente() {
		super("Saldo Insuficiente");
	}

}
