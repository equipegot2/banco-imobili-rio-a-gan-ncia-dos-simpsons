package br.ufc.qxd.es.pds.bancoimobiliario.view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IBancoImobiliario;
import br.ufc.qxd.es.pds.bancoimobiliario.view.TelaPrincipal;

public class BotaoTerminarTurnoListener implements ActionListener {

	private IBancoImobiliario banco;
	private JButton botaoterminarTurno;
	private JButton botaoFazerJogada;
	private TelaPrincipal infortativo;

	public BotaoTerminarTurnoListener(IBancoImobiliario banco, JButton botaoterminarTurno, JButton botaoFazerJogada, TelaPrincipal infortativo) {
		this.banco = banco;
		this.botaoterminarTurno = botaoterminarTurno;
		this.botaoFazerJogada = botaoFazerJogada;
		this.infortativo = infortativo;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		banco.concluirJogada();
		botaoterminarTurno.setEnabled(false);
		botaoFazerJogada.setEnabled(true);
		infortativo.desabilitarCompra();
	}

}
