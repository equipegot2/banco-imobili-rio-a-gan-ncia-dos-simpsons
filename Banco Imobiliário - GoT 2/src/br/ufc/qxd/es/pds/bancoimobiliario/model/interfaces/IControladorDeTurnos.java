package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;

import java.util.List;

public interface IControladorDeTurnos extends IControladorDeTurnosObservavel{
	
	public IJogador jogadorDaVez();
	
	public void finalizarTurno();
	
	public void iniciar(List<IJogador> jogadores);
	
	public List<IJogador> listarJogadores();
	
}
