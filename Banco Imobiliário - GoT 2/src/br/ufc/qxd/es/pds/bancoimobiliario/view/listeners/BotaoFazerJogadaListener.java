package br.ufc.qxd.es.pds.bancoimobiliario.view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IBancoImobiliario;

public class BotaoFazerJogadaListener implements ActionListener {

	private IBancoImobiliario banco;
	private JButton botao;
	private JButton origem;

	public BotaoFazerJogadaListener(IBancoImobiliario banco, JButton botao, JButton origem) {
		this.banco = banco;
		this.botao = botao;
		this.origem = origem;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int valorDados;
		String valorJogada;
		
		valorJogada = JOptionPane.showInputDialog("Digite o valor da soma dos dados!");
		
		try {
			valorDados = Integer.parseInt(valorJogada);
			banco.fazerJogada(valorDados);
			botao.setEnabled(true);
			origem.setEnabled(false);
		} catch (Exception e2) {
			JOptionPane.showMessageDialog(null, "Jogada invalida! Tente novamente.");
		}
	}

}
