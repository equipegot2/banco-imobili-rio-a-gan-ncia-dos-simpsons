package br.ufc.qxd.es.pds.bancoimobiliario.model;

import javax.swing.JOptionPane;

import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.IJogador;

public class LogradouroPontoDePartidaImp extends LogradouroAbstrato {

	@Override
	public void jogadorChegando(IJogador jogador) {
		comportamento.aplicarEfeito(jogador);
		JOptionPane.showMessageDialog(null, "Parabens, chegou ao ponto de Partida, receba 200!");
	}

	@Override
	public void jogadorPassando(IJogador jogador) {
		comportamento.aplicarEfeito(jogador);
		JOptionPane.showMessageDialog(null, "Parabens, Passou pelo ponto de Partida, receba 200!");
	}
}
