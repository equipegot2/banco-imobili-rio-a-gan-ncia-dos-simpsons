package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;


public interface IObservadorDeTabuleiro {

	void jogadorMovimentado(IJogador jogador);
}
