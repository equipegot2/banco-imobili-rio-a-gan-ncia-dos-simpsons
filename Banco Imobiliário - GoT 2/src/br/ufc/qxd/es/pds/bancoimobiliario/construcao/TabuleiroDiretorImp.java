package br.ufc.qxd.es.pds.bancoimobiliario.construcao;

import java.io.File;
import java.util.Map;

import br.ufc.qxd.es.pds.bancoimobiliario.exceptions.ExcecaoCarregamentoDeDados;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroEmpresaImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroImovelImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroDeslocamentoImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroNeutroImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroPontoDePartidaImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.LogradouroRevesImp;
import br.ufc.qxd.es.pds.bancoimobiliario.model.comportamento.ComportamentoDebitarSaldo;
import br.ufc.qxd.es.pds.bancoimobiliario.model.comportamento.ComportamentoDeslocamento;
import br.ufc.qxd.es.pds.bancoimobiliario.model.comportamento.ComportamentoIncrementarSaldo;
import br.ufc.qxd.es.pds.bancoimobiliario.model.enumeration.IdentificadorDeLogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ILogradouro;
import br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces.ITabuleiro;

import com.google.inject.Inject;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class TabuleiroDiretorImp implements ITabuleiroDiretor{
	
	private Map<Integer,ILogradouro> logradouros;
	private XStream xStream;
	
	@Inject
	private ITabuleiroConstrutor tabuleiroBuilder;
	
	private final String caminhoDoArquivoDeConfiguracao = "Resources/LogradourosConfig.xml";
	
	public TabuleiroDiretorImp() {
		configurarXStream();
		carregarDadosDeArquivo();
	}
	
	@Override
	public void construir() {
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.ESPECIAL_PONTO_DE_PARTIDA.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.CASA_DO_FLANDERS.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.CASA_DOS_SIMPSONS.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.EMPRESA_KRUST_BURG.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.ESPECIAL_CASA_NEUTRA.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.EMPRESA_BAR_DO_MOE.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.EMPRESA_CANAL6.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.CASA_CLASSE_POBRE.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.CASA_DO_BURNS.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.EMPRESA_USINA.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.ESPECIAL_DESLOCAMENTO.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.CASA_CLASSE_RICA.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.EMPRESA_MERCADINHO_DO_ABU.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.CASA_CLASSE_MEDIA.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.ESPECIAL_CASA_REVES.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.EMPRESA_PET_SHOP.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.CASA_CLASSE_MUITO_RICA.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.EMPRESA_DOCERIA.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.CASA_DA_KRABAPELL.getValor()));
		tabuleiroBuilder.adicionarLogradouro(logradouros.get(IdentificadorDeLogradouro.EMPRESA_CERVEJARIA_DUFF.getValor()));
	}

	@Override
	public ITabuleiro getTabuleiro() {
		return tabuleiroBuilder.getTabuleiro();
	}
	
	public ILogradouro getLogradouro(int i){
		return logradouros.get(i);
	}
	
	private void configurarXStream() {
		
		xStream = new XStream(new DomDriver());
		xStream.alias("Map", Map.class);
		xStream.alias("Imovel",LogradouroImovelImp.class);
		xStream.alias("Empresa",LogradouroEmpresaImp.class);
		xStream.alias("PontoInical",LogradouroPontoDePartidaImp.class);
		xStream.alias("LogradouroReves", LogradouroRevesImp.class);
		xStream.alias("LogradouroNeuto", LogradouroNeutroImp.class);
		xStream.alias("LogradouroDeslocamento",LogradouroDeslocamentoImp.class);
		xStream.alias("ComportamentoDebitarSaldo", ComportamentoDebitarSaldo.class);
		xStream.alias("ComportamentoDeslocamento", ComportamentoDeslocamento.class);
		xStream.alias("ComportamentoIncrementarSaldo", ComportamentoIncrementarSaldo.class);
		
	}
	
	private void carregarDadosDeArquivo(){
		
		
		File file = new File(caminhoDoArquivoDeConfiguracao);
		
		if(file.exists()){
		
			logradouros =  (Map) xStream.fromXML(file);
		}else{
			throw new ExcecaoCarregamentoDeDados();
		}
		
		
	}

	
}
