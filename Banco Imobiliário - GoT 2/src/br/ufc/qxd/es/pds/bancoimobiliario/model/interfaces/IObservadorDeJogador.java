package br.ufc.qxd.es.pds.bancoimobiliario.model.interfaces;

public interface IObservadorDeJogador {

	void jogadorDeslocado(IJogador sujeito);

	void jogadorMovimento(IJogador sujeito);

	void saldoAtualizado(IJogador jogadorImp);

}
