package br.ufc.qxd.es.pds.bancoimobiliario.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class TelaPrincipalModelo extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	
	public TelaPrincipalModelo() {
		setBackground(Color.YELLOW);
		setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 544, 881, 2);
		add(separator);
		
		JButton botaoIniciarPartida = new JButton("INICIAR PARTIDA");
		botaoIniciarPartida.setBounds(64, 559, 140, 45);
		add(botaoIniciarPartida);
		
		JButton btnNewButton = new JButton("AJUDA");
		btnNewButton.setBounds(268, 559, 140, 45);
		add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("FINALIZAR JOGADA");
		btnNewButton_1.setBounds(472, 559, 140, 45);
		add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("FAZER JOGADA");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnNewButton_2.setBounds(676, 559, 140, 45);
		add(btnNewButton_2);
		
		JButton botaoCasaEspecialInicio = new JButton("");
		botaoCasaEspecialInicio.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Especial Springfield.png")));
		botaoCasaEspecialInicio.setBounds(10, 455, 91, 78);
		add(botaoCasaEspecialInicio);
		
		JButton botaoImovelCasaDosFlanders = new JButton("");
		botaoImovelCasaDosFlanders.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Imovel Casa do Flanders.png")));
		botaoImovelCasaDosFlanders.setBounds(10, 366, 89, 78);
		add(botaoImovelCasaDosFlanders);
		
		JButton botaoImovelCasaDosSimpsons = new JButton("");
		botaoImovelCasaDosSimpsons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		botaoImovelCasaDosSimpsons.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Imovel Casa dos Simpsons.png")));
		botaoImovelCasaDosSimpsons.setBounds(10, 275, 89, 78);
		add(botaoImovelCasaDosSimpsons);
		
		JButton botaoEmpresaKrustBurg = new JButton("");
		botaoEmpresaKrustBurg.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Empresa Krust Burger.png")));
		botaoEmpresaKrustBurg.setBounds(10, 186, 89, 78);
		add(botaoEmpresaKrustBurg);
		
		JButton botaoCasaEspecialFolga = new JButton("");
		botaoCasaEspecialFolga.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Especial Folga.png")));
		botaoCasaEspecialFolga.setBounds(10, 100, 89, 78);
		add(botaoCasaEspecialFolga);
		
		JButton botaoEmpresaBarDoMoe = new JButton("");
		botaoEmpresaBarDoMoe.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Empresa Bar do Moe.png")));
		botaoEmpresaBarDoMoe.setBounds(10, 11, 89, 78);
		add(botaoEmpresaBarDoMoe);
		
		JButton botaoEmpresaCervejariaDuff = new JButton("");
		botaoEmpresaCervejariaDuff.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Empresa Cervejaria Duff.png")));
		botaoEmpresaCervejariaDuff.setBounds(111, 455, 91, 78);
		add(botaoEmpresaCervejariaDuff);
		
		JButton botaoImovelCasaDaKrabapell = new JButton("");
		botaoImovelCasaDaKrabapell.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Imovel Casa da Krabapel.png")));
		botaoImovelCasaDaKrabapell.setBounds(212, 455, 91, 78);
		add(botaoImovelCasaDaKrabapell);
		
		JButton botaoEmpresaDoceria = new JButton("");
		botaoEmpresaDoceria.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Empresa Doceria.png")));
		botaoEmpresaDoceria.setBounds(313, 455, 91, 78);
		add(botaoEmpresaDoceria);
		
		JButton botaoImovelCasa4 = new JButton("");
		botaoImovelCasa4.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Imovel Casa4.png")));
		botaoImovelCasa4.setBounds(414, 455, 91, 78);
		add(botaoImovelCasa4);
		
		JButton botaoEmpresaPetShop = new JButton("");
		botaoEmpresaPetShop.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Empresa Petshop.png")));
		botaoEmpresaPetShop.setBounds(515, 455, 89, 78);
		add(botaoEmpresaPetShop);
		
		JButton botaoCasaEspecialHomerDoh = new JButton("");
		botaoCasaEspecialHomerDoh.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Especial Homer Doh.png")));
		botaoCasaEspecialHomerDoh.setBounds(515, 366, 89, 78);
		add(botaoCasaEspecialHomerDoh);
		
		JButton botaoImovelCasa3 = new JButton("");
		botaoImovelCasa3.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Imovel Casa3.png")));
		botaoImovelCasa3.setBounds(515, 275, 89, 78);
		add(botaoImovelCasa3);
		
		JButton botaoEmpresaMercadinhoDoAbu = new JButton("");
		botaoEmpresaMercadinhoDoAbu.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Empresa Mercadinho do Abu.png")));
		botaoEmpresaMercadinhoDoAbu.setBounds(515, 186, 89, 78);
		add(botaoEmpresaMercadinhoDoAbu);
		
		JButton butaoImovelCasa2 = new JButton("");
		butaoImovelCasa2.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Imovel Casa2.png")));
		butaoImovelCasa2.setBounds(515, 100, 89, 78);
		add(butaoImovelCasa2);
		
		JButton botaoCasaEspecialHomerVoando = new JButton("");
		botaoCasaEspecialHomerVoando.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Especial Homer Voando.png")));
		botaoCasaEspecialHomerVoando.setBounds(515, 11, 89, 78);
		add(botaoCasaEspecialHomerVoando);
		
		JButton botaoEmpresaUsina = new JButton("");
		botaoEmpresaUsina.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Empresa Usina.png")));
		botaoEmpresaUsina.setBounds(416, 11, 89, 78);
		add(botaoEmpresaUsina);
		
		JButton botaoImovelMassaoDoBurns = new JButton("");
		botaoImovelMassaoDoBurns.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Imovel Mansão do Burns.png")));
		botaoImovelMassaoDoBurns.setBounds(315, 11, 89, 78);
		add(botaoImovelMassaoDoBurns);
		
		JButton botaoImovelCasa1 = new JButton("");
		botaoImovelCasa1.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Imovel Casa1.png")));
		botaoImovelCasa1.setBounds(214, 11, 89, 78);
		add(botaoImovelCasa1);
		
		JButton botaoEmpresaCanal6 = new JButton("");
		botaoEmpresaCanal6.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/Empresa Canal 6.png")));
		botaoEmpresaCanal6.setBounds(111, 11, 91, 78);
		add(botaoEmpresaCanal6);
		
		JLabel labelTextoJogador1 = new JLabel("JOGADOR 1");
		labelTextoJogador1.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoJogador1.setBounds(625, 11, 91, 14);
		add(labelTextoJogador1);
		
		JLabel labelFotoJogador1 = new JLabel("");
		labelFotoJogador1.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/fotoJogador1.png")));
		labelFotoJogador1.setBounds(625, 33, 91, 78);
		add(labelFotoJogador1);
		
		JButton botaoLocalAtualJogador1 = new JButton("POSIÇÃO");
		botaoLocalAtualJogador1.setBounds(726, 33, 91, 78);
		add(botaoLocalAtualJogador1);
		
		JLabel labelTextoSaldoJogador1 = new JLabel("SALDO R$:");
		labelTextoSaldoJogador1.setHorizontalAlignment(SwingConstants.RIGHT);
		labelTextoSaldoJogador1.setBounds(625, 122, 91, 14);
		add(labelTextoSaldoJogador1);
		
		JLabel labelValorSaldoJogador1 = new JLabel("1000.00");
		labelValorSaldoJogador1.setHorizontalAlignment(SwingConstants.LEFT);
		labelValorSaldoJogador1.setBounds(726, 122, 90, 14);
		add(labelValorSaldoJogador1);
		
		JButton botaoVerPropriedadesJogador1 = new JButton("VER PROPRIEDADES");
		botaoVerPropriedadesJogador1.setBounds(625, 147, 191, 23);
		add(botaoVerPropriedadesJogador1);
		
		JLabel labelTextoLocalAtualJogador1 = new JLabel("LOCAL ATUAL");
		labelTextoLocalAtualJogador1.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoLocalAtualJogador1.setBounds(726, 11, 90, 14);
		add(labelTextoLocalAtualJogador1);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(614, 186, 217, 2);
		add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(614, 351, 217, 2);
		add(separator_2);
		
		JLabel labelTextoJogador2 = new JLabel("JOGADOR 2");
		labelTextoJogador2.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoJogador2.setBounds(624, 374, 91, 14);
		add(labelTextoJogador2);
		
		JLabel labelTextoLocalAtualJogador2 = new JLabel("LOCAL ATUAL");
		labelTextoLocalAtualJogador2.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextoLocalAtualJogador2.setBounds(725, 374, 90, 14);
		add(labelTextoLocalAtualJogador2);
		
		JButton btnPosioAtualDo = new JButton("POSIÇÃO");
		btnPosioAtualDo.setBounds(725, 396, 91, 78);
		add(btnPosioAtualDo);
		
		JLabel labelFotoJogador2 = new JLabel("");
		labelFotoJogador2.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/fotoJogador2.png")));
		labelFotoJogador2.setBounds(624, 396, 91, 78);
		add(labelFotoJogador2);
		
		JLabel labelTextoSaldoJogador2 = new JLabel("SALDO R$:");
		labelTextoSaldoJogador2.setHorizontalAlignment(SwingConstants.RIGHT);
		labelTextoSaldoJogador2.setBounds(624, 485, 91, 14);
		add(labelTextoSaldoJogador2);
		
		JLabel labelValorSaldoJogador2 = new JLabel("1000.00");
		labelValorSaldoJogador2.setHorizontalAlignment(SwingConstants.LEFT);
		labelValorSaldoJogador2.setBounds(725, 485, 90, 14);
		add(labelValorSaldoJogador2);
		
		JButton botaoVerPropriedadesJogador2 = new JButton("VER PROPRIEDADES");
		botaoVerPropriedadesJogador2.setBounds(624, 510, 191, 23);
		add(botaoVerPropriedadesJogador2);
		
		JLabel labelTextoJogadorDaVez = new JLabel("JOGADOR DA VEZ:");
		labelTextoJogadorDaVez.setBounds(625, 266, 91, 14);
		add(labelTextoJogadorDaVez);
		
		JLabel labelFotoJogadorDaVez = new JLabel("");
		labelFotoJogadorDaVez.setIcon(new ImageIcon(TelaPrincipalModelo.class.getResource("/icones/fotoJogador1.png")));
		labelFotoJogadorDaVez.setBounds(726, 231, 90, 78);
		add(labelFotoJogadorDaVez);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 255), 5));
		panel.setBounds(109, 101, 396, 343);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblInserirAFoto = new JLabel("inserir a foto do logradouro aqui");
		lblInserirAFoto.setHorizontalAlignment(SwingConstants.CENTER);
		lblInserirAFoto.setBounds(10, 11, 220, 206);
		panel.add(lblInserirAFoto);
		
		JButton btnNewButton_3 = new JButton("COMPRAR");
		btnNewButton_3.setBounds(10, 309, 376, 23);
		panel.add(btnNewButton_3);
		
		JLabel lblNewLabel = new JLabel("inserir no nome do logradouro");
		lblNewLabel.setBounds(240, 11, 146, 14);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("valor do logradouro se houver");
		lblNewLabel_1.setBounds(240, 36, 146, 14);
		panel.add(lblNewLabel_1);
		
		JLabel lblTaxaDoLogradouro = new JLabel("taxa do logradouro se houver");
		lblTaxaDoLogradouro.setBounds(240, 61, 146, 14);
		panel.add(lblTaxaDoLogradouro);
		
		JLabel lblDonoDoLogradouro = new JLabel("dono se houver");
		lblDonoDoLogradouro.setHorizontalAlignment(SwingConstants.CENTER);
		lblDonoDoLogradouro.setBounds(270, 86, 91, 78);
		panel.add(lblDonoDoLogradouro);
		
		JLabel lblNewLabel_2 = new JLabel("inserir descrição do logradouro aqui");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(10, 228, 376, 70);
		panel.add(lblNewLabel_2);

	}
}
